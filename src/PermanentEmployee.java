public class PermanentEmployee extends  Employee {

    private int noOfVacationDays;
    private double salary;

    public PermanentEmployee(String name, String department) {
        super(name, department);
        noOfVacationDays = 50;
    }

    @Override
    public int applyForLeave(int noOfDays) {
        if (this.noOfVacationDays - noOfDays > 0) {
            this.noOfVacationDays -= noOfDays;
            return noOfDays;
        }
        return 0;
    }
}