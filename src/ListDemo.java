import java.util.Collections;
import java.util.List;

public class ListDemo {

    public static void main(String[] args) {
        final List<String> numbers = List.of("one", "two", "three", "four", "five");
        Collections.sort(numbers);
    }
}