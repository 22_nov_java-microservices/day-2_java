package streams;

import com.workshop.model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static streams.Brand.*;
import static streams.ProcessingFilters.printBrand;

public class CleanCode {

    // Java 8 - Streams API
     /*
         - Mathematical formulae
            (a + b) pow 2 = a2 + b2 + 2 * a * b

         - Functional interfaces
           Functional descriptors

           - Predicate
             - input -> value, output -> boolean
           - Consumer
              - input -> value, output -> void
           - Supplier
              - input -> no , output -> value
           - Function
              - input -> value, output -> value
           - BiFunction
              - input -> value,value , output -> value
      */

/*    Predicate<Product> isIPhone = new Predicate<Product>() {
        @Override
        public boolean test(Product product) {
            return product.getBrand().equals(APPLE);
        }
    };*/

    // datatype variableName = value;
    // For Functional interfaces
    // datatype variableName = behavior
    static Predicate<Product> isIPhone = product -> product.getBrand().equals(APPLE);
    static Predicate<Product> isSamsung = product -> product.getBrand().equals(SAMSUNG);
    static Predicate<Product> isOPPO = (product) -> product.getBrand().equals(OPPO);
    static Predicate<Product> isMacBookPro = product -> product.getName().equals("MacBookPro");
    static Predicate<Product> isLessThan50k = product -> product.getPrice() < 50_000;

    static Predicate<Product> isMoreThan50k = isLessThan50k.negate();
    static  Predicate<Product> isMoreThan50KAndApple = isMoreThan50k.and(isIPhone.or(isMacBookPro));


    static Predicate<Product> appleProduct = isIPhone.or(isMacBookPro);
    static Predicate<Product> nonAppleProduct = isIPhone.or((product -> product.getName().equals("MacBookPro"))).negate();

    static Function<Product, String> productToProductName = product -> product.getName();


    public static void main(String[] args) {

        Product iPhone = new Product("I-Phone", 2021, 1_50_000, APPLE);
        Product macBookPro = new Product("MacBookPro", 2019, 1_15_000, APPLE);
        Product galaxy = new Product("Samsung-Galaxy", 2017, 75_000, SAMSUNG);
        Product oppo_F17Pro = new Product("Oppo-F17-Pro", 2020, 15_000, OPPO);
        Product s20 = new Product("Samsung", 2021, 80_000, SAMSUNG);

        final List<Product> products = List.of(iPhone, macBookPro, galaxy, oppo_F17Pro, s20);

        //write the code to filter out the products by brand APPLE
        //data store
        List<Product> appleProducts  = new ArrayList<>();

        Predicate<Product> isIPhone = product -> product.getBrand().equals(APPLE);
        Predicate<Product> isSamsung = product -> product.getBrand().equals(SAMSUNG);
        Predicate<Product> isOPPO = (product) -> product.getBrand().equals(OPPO);
        Predicate<Product> isMacBookPro = product -> product.getName().equals("MacBookPro");
        Predicate<Product> isLessThan50k = product -> product.getPrice() < 50_000;

        Predicate<Product> isMoreThan50k = isLessThan50k.negate();
        Predicate<Product> isMoreThan50KAndApple = isMoreThan50k.and(isIPhone.or(isMacBookPro));




        //processing logic
        //imperative - explicitly tell how to do
       /* for(Product product: products){
            // condition - boolean
            if(product.getBrand().equals(APPLE)){
                appleProducts.add(product);
            }
        }

        for(Product product: appleProducts){
            System.out.println(product);
        }*/

        //Using Streams API
        //System.out.println(isIPhone.test(iPhone));
     /*   products
                .stream()
                .filter(isIPhone)
                .map(product -> product.getName())
                .forEach(name -> System.out.println("Product name "+ name));
        */

/*
        products.stream()
                .filter(isIPhone.or((product -> product.getName().equals("MacBookPro")))).map(product -> product.getName()).forEach(name -> System.out.println(name));
*/
        //products.stream().filter(nonAppleProduct).map(Product::getName).forEach(System.out::println);
        //products.stream().filter(nonAppleProduct).map(Product::getBrand).map(brand -> brand.name()).forEach(printBrand);

        final Product product = products.stream().max((product1, product2) -> Double.compare(product1.getPrice(), product2.getPrice())).get();
        System.out.println("Product with Max price " + product.getName());

        final List<Product> appleProducts1 = products.stream().filter(appleProduct).collect(Collectors.toList());
        
        appleProducts.stream().forEach(product1 -> System.out.println(product1.getName()));

    }
}