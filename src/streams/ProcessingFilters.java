package streams;

import com.workshop.model.Product;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import static streams.Brand.*;

public class ProcessingFilters {

    public static Predicate<Product> isIPhone = product -> product.getBrand().equals(APPLE);
    public static Predicate<Product> isSamsung = product -> product.getBrand().equals(SAMSUNG);
    public static Predicate<Product> isOPPO = (product) -> product.getBrand().equals(OPPO);
    public static Predicate<Product> isMacBookPro = product -> product.getName().equals("MacBookPro");
    public static Predicate<Product> isLessThan50k = product -> product.getPrice() < 50_000;
    public static Predicate<Product> isMoreThan50k = isLessThan50k.negate();
    public static  Predicate<Product> isMoreThan50KAndApple = isMoreThan50k.and(isIPhone.or(isMacBookPro));
    public static Predicate<Product> appleProduct = isIPhone.or(isMacBookPro);
    public static Predicate<Product> nonAppleProduct = isIPhone.or((product -> product.getName().equals("MacBookPro"))).negate();
    public static Function<Product, String> productToProductName = product -> product.getName();
    public static Consumer<String> printBrand = (brand) -> System.out.println("Product Brand "+ brand);
}