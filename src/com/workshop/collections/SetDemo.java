package com.workshop.collections;

import com.workshop.model.Order;

import java.util.HashSet;
import java.util.Set;

/*
   - Set do not allow duplicates
   - Set is not index based

   - Equality
     - Equality by reference
     - Equality by content
 */
public class SetDemo {
    public static void main(String[] args) {

        Order order1 = new Order(12, 25000, "Vinod");
        Order order2 = new Order(12, 25000, "Vinod");

        //by reference
        System.out.println("Are the two orders equal by reference: " + (order1 == order2));

        //by content
        System.out.println("Are the two orders equal by content :: " + order1.equals(order2));

        Set<Order> orders = new HashSet<>();

        orders.add(order1);
        orders.add(order2);
        orders.add(order1);
        orders.add(order2);
        orders.add(order1);
        orders.add(order2);
        orders.add(order1);
        orders.add(order2);
        System.out.println("Elements in orders :: " + orders.size());
    }
}