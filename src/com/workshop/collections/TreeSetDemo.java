package com.workshop.collections;

import com.workshop.model.Order;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class TreeSetDemo {
    public static void main(String[] args) {
        Set<Order> orders = new TreeSet<>();

        Order order1 = new Order(1111, 100, "Anuj");
        Order order2 = new Order(1200, 200, "Aravind");
        Order order3 = new Order(1300, 50, "Zeenat");
        Order order4 = new Order(500, 2500, "Hary");

        orders.add(order1);
        orders.add(order2);
        orders.add(order3);
        orders.add(order4);

        System.out.println("Size of Orders set :: "+ orders.size());

        Iterator<Order> it = orders.iterator();
        while(it.hasNext()){
            System.out.println(it.next());
        }

    }
}