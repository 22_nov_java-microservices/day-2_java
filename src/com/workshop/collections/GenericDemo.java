package com.workshop.collections;

/*
interface ProcessData {

      Number process(Object data);
}

class ProcessString implements ProcessData{

    @Override
    public Integer process(Object data) {
        return Integer.parseInt((String)data);
    }
}
class ProcessOrder implements ProcessData{

    @Override
    public Long process(Object data) {
        return ((Order) data).getOrderId();
    }
}
*/

import com.workshop.model.Order;

interface ProcessData<T, V> {

     V process(T data);

}

class ProcessDataString implements ProcessData<String,Integer>{

    @Override
    public  Integer process(String data) {
        return data.length();
    }
}

class ProcessOrderData implements ProcessData<Order, Long> {

    @Override
    public Long process(Order order) {
        return order.getOrderId();
    }
}


public class GenericDemo {

    public static void main(String[] args) {

    }
}