package com.workshop.collections;

import com.workshop.model.Order;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class TreeSetComparatorDemo {
    public static void main(String[] args) {
/*
        Comparator<Order> orderComparator = new Comparator<Order>() {
            @Override
            public int compare(Order order1, Order order2) {
                return  (int)(order1.getOrderId() - order2.getOrderId());
            }
        };
*/

/*

        Comparator<Order> orderComparator = (Order order1, Order order2) ->{
                return  (int)(order1.getOrderId() - order2.getOrderId());
            };
        };
*/
        Comparator<Order> orderComparator = (order1, order2) ->  (int)(order1.getOrderId() - order2.getOrderId());
        Comparator<Order> comparatorByIdDesc = (o1, o2) -> (int)(o2.getOrderId() - o1.getOrderId());
        Set<Order> orders = new TreeSet<>(orderComparator);

        Order order1 = new Order(1111, 100, "Anuj");
        Order order2 = new Order(1200, 200, "Aravind");
        Order order3 = new Order(1300, 50, "Zeenat");
        Order order4 = new Order(500, 2500, "Hary");
        orders.add(order1);
        orders.add(order2);
        orders.add(order3);
        orders.add(order4);

        System.out.println("Size of Orders set :: "+ orders.size());
        Iterator<Order> it = orders.iterator();
        while(it.hasNext()){
            it.next().getCustomerName();
        }
    }
}

//Comparator

class OrderComparatorByIdAsc implements Comparator<Order> {
    @Override
    public int compare(Order order1, Order order2) {
        return  (int)(order1.getOrderId() - order2.getOrderId());
    }
}

class OrderComparatorByIdDsc implements Comparator<Order> {

    @Override
    public int compare(Order order1, Order order2) {
        return  (int)(order2.getOrderId() - order1.getOrderId());
    }
}
class OrderComparatorByNameAsc implements Comparator<Order> {

    @Override
    public int compare(Order order1, Order order2) {
        return  (int)(order1.getCustomerName().compareTo(order2.getCustomerName()));
    }
}

class OrderComparatorByNameDsc implements Comparator<Order> {

    @Override
    public int compare(Order order1, Order order2) {
        return  (int)(order2.getCustomerName().compareTo(order1.getCustomerName()));
    }
}

class OrderComparatorByPriceAsc implements Comparator<Order> {

    @Override
    public int compare(Order order1, Order order2) {
        return  Double.compare(order1.getPrice(), order2.getPrice());
    }
}
class OrderComparatorByPriceDsc implements Comparator<Order> {

    @Override
    public int compare(Order order1, Order order2) {
        return  Double.compare(order2.getPrice(), order1.getPrice());
    }
}