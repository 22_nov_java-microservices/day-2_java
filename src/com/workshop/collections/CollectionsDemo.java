package com.workshop.collections;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CollectionsDemo {
    public static void main(String[] args) {
        /*
                                Collection
                                  |
                                  |
                                Iterable
                                  |
                                  |
            List<E>                Set<E>                     Queue<E>                   Map<K,V>
             - ArrayList        - HashSet               - PriorityQueue         - HashMap
             - LinkedLIst       - LinkedHashSet                                 - TreeMap
                               SortedSet
                                - TreeSet
         */

        int[] arrayElements = {12, 34, 55, 66};

        List<String> list = new LinkedList<>();
        list.add("one");
        list.add("two");

        processList(list);

        list.get(0);
        list.clear();;
        list.size();
        list.isEmpty();
    }

    private static void processList(List<String> list) {

        //1st way
        for (int index = 0; index < list.size(); index++) {
            System.out.println(list.get(index));
        }

        //2nd way
        for (String data: list) {
            System.out.println(data);
        }
    }
}