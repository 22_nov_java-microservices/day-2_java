package com.workshop.model;


import java.util.Objects;

public class Order implements Comparable<Order>{

    private long orderId;

    private double price;

    private String customerName;

    public Order(long orderId, double price, String customerName) {
        this.orderId = orderId;
        this.price = price;
        this.customerName = customerName;
    }
    public long getOrderId() {
        return orderId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return orderId == order.orderId && Double.compare(order.price, price) == 0 && Objects.equals(customerName, order.customerName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, price, customerName);
    }

    @Override
    /*
        Default sort for TreeSet implementations.
         positive number -> current object is greater than the passed object
         negative number -> passed object is greater then the current object
         zero -> both objects are same
     */
    public int compareTo(Order order) {
        //comparing the orders by orderid
         //return (int)(order.orderId - this.getOrderId());
       // return order.customerName.compareTo(this.customerName);
        return Double.compare(this.getPrice(), order.price) * -1;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", price=" + price +
                ", customerName='" + customerName + '\'' +
                '}';
    }
}