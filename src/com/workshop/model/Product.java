package com.workshop.model;

import streams.Brand;

public class Product {
    private final String name;
    private final int model;
    private final double price;
    private final Brand brand;

    public Product(String name, int model, double price, Brand brand) {
        this.name = name;
        this.model = model;
        this.price = price;
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public int getModel() {
        return model;
    }

    public double getPrice() {
        return price;
    }

    public Brand getBrand() {
        return brand;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", model=" + model +
                ", price=" + price +
                ", brand=" + brand +
                '}';
    }
}