package com.workshop.package1;

//only public and default at the class level
public class Server {

    private void privateMethod(){
        System.out.println("Private method");
    }

    protected  void protectedMethod(){
        System.out.println("Protected method");
    }

    void defaultMethod(){
        System.out.println("Default method");
    }

    public void publicMethod(){
        System.out.println("Public method");
    }

    public static void staticMethod(){

    }
}