package com.workshop.package1;

public class CallerFromSamePackage {

    public static void main(String[] args) {
        Server obj = new Server();
        //Not visible
        //obj.privateMethod();

        obj.defaultMethod();
        obj.protectedMethod();
        obj.publicMethod();
    }
}