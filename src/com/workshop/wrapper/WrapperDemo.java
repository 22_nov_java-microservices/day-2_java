package com.workshop.wrapper;

public class WrapperDemo {
    public static void main(String[] args) {
        //boxing
        Integer value = 45;
        int intValue = 45;
        Integer.parseInt("23");
        Integer.min(45, 67);
        Integer.max(45, 67);
        //unboxing
        int integerValue = value;

        //wrapper classes for all primitive
        Boolean flag = false;
        Float floatValue = 56.90F;
        Double doubleValue = 567.89;
    }
}