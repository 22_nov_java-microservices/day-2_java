package com.workshop;

import com.workshop.model.Order;
import com.workshop.model.Payment;

import java.util.List;

public interface CRUDDAO<T, E> {

    T save(T arg0);

    List<T> findAll();

    T findById(E arg0);

    void deleteById(E arg0);
}


class OrdersDAO implements CRUDDAO<Payment, String> {

    @Override
    public Payment save(Payment payment) {
        return null;
    }

    @Override
    public List<Payment> findAll() {
        return null;
    }

    @Override
    public Payment findById(String s) {
        return null;
    }

    @Override
    public void deleteById(String s) {

    }
}
