package com.workshop.package2;

import com.workshop.package1.Server;

import static com.workshop.package1.Server.staticMethod;

public class ClassFromAnotherPackage extends Server {

    private final double PI = 3.142;

    public static void main(String[] args) {
        //Fully qualified class name
        Server server = new Server();
        com.workshop.package3.Server package3Server = new com.workshop.package3.Server();
        server.publicMethod();
        //can access the protected method only through inheritance
        ClassFromAnotherPackage obj = new ClassFromAnotherPackage();
        obj.protectedMethod();
        staticMethod();
    }
}