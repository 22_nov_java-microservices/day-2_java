package com.workshop;

import com.workshop.model.Order;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class HashSetDemo {

    public static void main(String[] args) {
        Set<Order> orders = new HashSet<>();

        Order order1 = new Order(1234, 10000, "Vinay");
        Order order2 = new Order(12345, 10000, "Vinay");

        System.out.println("Hashcode of order1 " +order1.hashCode());
        System.out.println("Hashcode of order2 " +order2.hashCode());
        System.out.println("Are two orders same "+ order1.equals(order2));
        orders.add(order1);
        orders.add(order2);

        System.out.println("Size of Orders " + orders.size());
        System.out.println("CHeck if order1 is present in the hashset :: "+ orders.contains(order1));
        order1.setCustomerName("vikram");
        System.out.println("Hashcode of order 1 after updating customer name " +order1.hashCode());
        System.out.println("CHeck if order1 is present in the hashset second time :: "+ orders.contains(order1));

        final Iterator<Order> iterator = orders.iterator();
        while(iterator.hasNext()){
            final Order order = iterator.next();
            System.out.println("Order :: "+ order.getOrderId());
        }
    }
}