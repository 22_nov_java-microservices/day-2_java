import java.util.*;


interface PricingAlgorithm {

    double calculatePrice(double amount);

    default double applyGST(double amount){
       return amount + amount*18/100;
    }

    static double currentExchangeRate(double amountInINR){
        return amountInINR * .75;
    }
}

class ElectoricPricingAlgorithm implements PricingAlgorithm{

    @Override
    public double calculatePrice(double amount) {
        return amount + amount * 20/100;
    }
}

class ApparalPricingAlgorithm implements PricingAlgorithm{

    @Override
    public double calculatePrice(double amount) {
        return amount + amount * 40/100;
    }
}


public class DefaultMethodDemo {
    public static void main(String[] args) {
        PricingAlgorithm.currentExchangeRate(2000);
        final List<Integer> integerList = List.of(1, 2, 3, 4);

        //old way - verbose
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);


        List<String> strLIst = Arrays.asList("one", "two", "three", "four");

        //Native with List
        final List<String> stringList = List.of("one", "two");
    }
}