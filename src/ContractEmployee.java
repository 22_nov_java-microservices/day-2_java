public final class ContractEmployee extends  Employee {

    private int duration;

    public ContractEmployee(String name, String department) {
        super(name,department);
    }

    @Override
    public int applyForLeave(int noOfDays){
        this.duration += noOfDays;
        return noOfDays;
    }
}