

abstract class DBTemplate {

    public void processData(String connectionUrl, String username, String password){

        //1. create DB connection
        createConnection(connectionUrl, username, password);
        createStatement();
        execute();
        closeStatement();
        closeDBConnection();

    }

    private void closeDBConnection() {
        System.out.println("Closing the DB Connection ....");
    }

    private void closeStatement() {
        System.out.println("Closing the statement object ....");
    }

    protected abstract void execute();

    private void createStatement() {
        System.out.println("Creating the statement");
    }

    private void createConnection(String connectionUrl, String username, String password) {
        System.out.println("Creating the DB connection object");
    }
}


public class DBClient extends DBTemplate {
    public static void main(String[] args) {
        DBTemplate template = new DBClient();
        template.processData("jdbc:h2:mem", "root", "welcome");
    }

    @Override
    protected void execute() {
        System.out.println("Processing the records ::::");
    }
}