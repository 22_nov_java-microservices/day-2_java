@FunctionalInterface
interface PaymentGateway {

    boolean pay(String from, String to, double amount, String notes);
}

interface PrepaidRecharge {
    void recharge(String phoneNo, double amount);
}

class PhonePay implements PaymentGateway {

    @Override
    public boolean pay(String from, String to, double amount, String notes) {
        System.out.printf("Paying %s from %s, an amount of %s. Notes %s  with PhonePay", to, from, amount, notes);
        System.out.println(" You have earned "+ Math.ceil(Math.random()* 100) + " reward points");
        return false;
    }

}

class GooglePay implements PaymentGateway {
    @Override
    public boolean pay(String from, String to, double amount, String notes) {
        System.out.printf("Paying %s from %s, an amount of %s. Notes %s with GPay ", to, from, amount, notes);
        System.out.println(" You have earned "+ Math.ceil(Math.random()* 100) + " cashback");
        return false;
    }
}
public class MoneyTransfer {
    String data;

    public static void main(String[] args) {
        String option = "1";
        if (args != null && args.length > 0) {
            option = args[0];
        }
        //variable declared inside a method do not get a default value
        // You need to initialize them before they can be used
        PaymentGateway gateway = null;
        //declaration
        String data;
        if (option.equalsIgnoreCase("1")) {
            gateway = new GooglePay();
        } else {
            gateway = new PhonePay();
        }
        gateway.pay("Neeraj", "Vishal", 20000, "Thanks for the purchase");

        PaymentGateway gatewayImp = (from, to, amount, notes) -> (amount < 100) ? false : true;

    }
}