class MathUtil {
    public static int max(int a, int b){
        if (a > b){
            return  a;
        }
        return  b;
    }
    public static int min(int a, int b){
        if (a < b){
            return  a;
        }
        return  b;
    }
}

public class MathUtilClient {

    public static void main(String[] args) {
        System.out.printf(" Max " + MathUtil.max(23, 67));
    }
}

