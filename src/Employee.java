public abstract class Employee {

    private String name;

    private String department;


    public Employee(String name, String department){
        this.name = name;
        this.department = department;
    }

    public abstract int applyForLeave(int noOfDays);

    public final void commute(String pickupLocation){
        System.out.println(" providing commute facility :: ");
    }
}