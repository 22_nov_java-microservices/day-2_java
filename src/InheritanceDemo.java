
abstract class AbstractGrandParent {

    abstract void grandParent();
}


abstract class Parent extends  AbstractGrandParent {

    @Override
    void grandParent() {

    }

    abstract  void parent();
}

class Child extends Parent {

    static Child of(){
        return new Child();
    }


    @Override
    void parent() {

    }
}

public class InheritanceDemo {

    public static void main(String[] args) {
        final Child child = Child.of();
    }
}